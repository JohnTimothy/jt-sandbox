=======================================================================
PADS Professional Layout - Version 116.0.506.1217
=======================================================================

Job Directory:        C:\pd_repo\pd\_COWBOY\_Mentor\PCB\

Design Status Report: C:\pd_repo\pd\_COWBOY\_Mentor\PCB\LogFiles\DesignStatus_00.txt

Wed Feb 08 10:55:59 2017

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.02 X 75.48 (mm)
Route Border Extents  .......... 55.02 X 75.48 (mm)
Actual Board Area  ............. 3,025.83 (mm)
Actual Route Area  ............. 3,025.83 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    6,051.66 Sq. (mm) 643.99 Sq. (mm)   10.64 %

Pins  .......................... 460
Pins per Route Area  ........... 0.15 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.13, 0.13, 0.2, 0.3, 0.9, 1
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... None.
    Layer 4 is a signal layer
        Trace Widths  .......... 0.13, 0.2, 0.3

Nets  .......................... 97
Connections  ................... 390
Open Connections  .............. 0
Differential Pairs  ............ 0
Percent Routed  ................ 100.00 %

Netline Length  ................ 0 (mm)
Netline Manhattan Length  ...... 0 (mm)
Total Trace Length  ............ 1,679.35 (mm)

Trace Widths Used (mm)  ........ 0.13, 0.13, 0.2, 0.3, 0.9, 1
Vias  .......................... 377
Via Span  Name                   Quantity
   1-4    0.38VIAh0.2            377

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 193
    Parts Mounted on Top  ...... 174
        SMD  ................... 136
        Through  ............... 5
        Test Points  ........... 33
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 19
        SMD  ................... 0
        Through  ............... 0
        Test Points  ........... 19
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 398
    Holes per Board Area  ...... 0.13 Holes/Sq. (mm)
Mounting Holes  ................ 11
