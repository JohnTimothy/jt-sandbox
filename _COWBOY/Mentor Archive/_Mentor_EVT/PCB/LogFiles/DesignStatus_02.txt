=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_02.txt

Thu Sep 24 15:59:30 2015

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.97 X 61.11 (mm)
Route Border Extents  .......... 55.97 X 61.11 (mm)
Actual Board Area  ............. 1,674.55 (mm)
Actual Route Area  ............. 1,674.55 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,349.1 Sq. (mm)  1,257.65 Sq. (mm) 37.55 %

Pins  .......................... 479
Pins per Route Area  ........... 0.29 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.25, 0.25, 0.3, 0.35, 0.5, 1
    Layer 2 is a signal layer
        Trace Widths  .......... 0.13, 0.2
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.5, 1.4

Nets  .......................... 123
Connections  ................... 350
Open Connections  .............. 2
Differential Pairs  ............ 0
Percent Routed  ................ 99.43 %

Netline Length  ................ 2.22 (mm)
Netline Manhattan Length  ...... 2.82 (mm)
Total Trace Length  ............ 2,136.69 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.25, 0.25, 0.3, 0.35, 0.5, 1, 1.4
Vias  .......................... 191
Via Span  Name                   Quantity
   1-4    026VIA                 11
          0.3VIAh0.15            14
          0.38VIAh0.2            60
          0.38VIAh0.25           68
   1-2    0.38VIAh0.25           18
          0.3VIAh0.15            7
   1-3    0.38VIAh0.2            9
   3-4    0.38VIAh0.2            4

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 187
    Parts Mounted on Top  ...... 124
        SMD  ................... 81
        Through  ............... 4
        Test Points  ........... 39
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 63
        SMD  ................... 50
        Through  ............... 1
        Test Points  ........... 12
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 3

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 173
    Holes per Board Area  ...... 0.1 Holes/Sq. (mm)
Mounting Holes  ................ 11
