=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\HeshBT\Mentor\LHBT120Main\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\HeshBT\Mentor\LHBT120Main\PCB\LogFiles\DesignStatus_64.txt

Fri Apr 29 09:47:48 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 61.41 X 75.76 (mm)
Route Border Extents  .......... 60.68 X 75.01 (mm)
Actual Board Area  ............. 3,946.69 (mm)
Actual Route Area  ............. 3,852.33 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    7,893.37 Sq. (mm) 588.7 Sq. (mm)    7.46 %

Pins  .......................... 359
Pins per Route Area  ........... 0.09 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.13, 0.2, 0.4, 0.8
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... 0.2
    Layer 4 is a signal layer
        Trace Widths  .......... 0.13, 0.2

Nets  .......................... 78
Connections  ................... 291
Open Connections  .............. 4
Differential Pairs  ............ 0
Percent Routed  ................ 98.63 %

Netline Length  ................ 3.7 (mm)
Netline Manhattan Length  ...... 4.35 (mm)
Total Trace Length  ............ 1,352.57 (mm)

Trace Widths Used (mm)  ........ 0.13, 0.2, 0.4, 0.8
Vias  .......................... 174
Via Span  Name                   Quantity
   1-4    0.38VIAh0.2            174

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 142
    Parts Mounted on Top  ...... 119
        SMD  ................... 95
        Through  ............... 5
        Test Points  ........... 19
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 23
        SMD  ................... 0
        Through  ............... 0
        Test Points  ........... 23
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 197
    Holes per Board Area  ...... 0.05 Holes/Sq. (mm)
Mounting Holes  ................ 13
