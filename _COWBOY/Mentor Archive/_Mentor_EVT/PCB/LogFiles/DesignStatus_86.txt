=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\john.timothy\Desktop\LHBT120Main\PCB\

Design Status Report: C:\Users\john.timothy\Desktop\LHBT120Main\PCB\LogFiles\DesignStatus_86.txt

Wed May 11 13:52:37 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 61.41 X 75.76 (mm)
Route Border Extents  .......... 60.68 X 75.01 (mm)
Actual Board Area  ............. 3,946.69 (mm)
Actual Route Area  ............. 3,852.33 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    7,893.37 Sq. (mm) 631.53 Sq. (mm)   8.00 %

Pins  .......................... 408
Pins per Route Area  ........... 0.11 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.13, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.8
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... 0.2
    Layer 4 is a signal layer
        Trace Widths  .......... 0.13, 0.13, 0.2, 0.3

Nets  .......................... 89
Connections  ................... 349
Open Connections  .............. 1
Differential Pairs  ............ 0
Percent Routed  ................ 99.71 %

Netline Length  ................ 0 (mm)
Netline Manhattan Length  ...... 0 (mm)
Total Trace Length  ............ 1,518.45 (mm)

Trace Widths Used (mm)  ........ 0.13, 0.13, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.8
Vias  .......................... 213
Via Span  Name                   Quantity
   1-4    0.38VIAh0.2            213

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 163
    Parts Mounted on Top  ...... 138
        SMD  ................... 116
        Through  ............... 5
        Test Points  ........... 17
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 25
        SMD  ................... 0
        Through  ............... 0
        Test Points  ........... 25
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 236
    Holes per Board Area  ...... 0.06 Holes/Sq. (mm)
Mounting Holes  ................ 13
