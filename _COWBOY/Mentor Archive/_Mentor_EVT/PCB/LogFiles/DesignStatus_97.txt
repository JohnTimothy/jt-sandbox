=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\HeshBT\Mentor\LHBT120Main\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\HeshBT\Mentor\LHBT120Main\PCB\LogFiles\DesignStatus_97.txt

Fri Jun 10 11:50:19 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 61.42 X 75.86 (mm)
Route Border Extents  .......... 60.68 X 75.01 (mm)
Actual Board Area  ............. 3,951.31 (mm)
Actual Route Area  ............. 3,852.33 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    7,902.61 Sq. (mm) 631.75 Sq. (mm)   7.99 %

Pins  .......................... 412
Pins per Route Area  ........... 0.11 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.13, 0.2, 0.23, 0.25, 0.3, 0.4, 0.5, 0.6, 0.8
    Layer 2 is a signal layer
        Trace Widths  .......... None.
    Layer 3 is a signal layer
        Trace Widths  .......... 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.13, 0.13, 0.2, 0.3, 0.4, 0.5

Nets  .......................... 92
Connections  ................... 350
Open Connections  .............. 0
Differential Pairs  ............ 0
Percent Routed  ................ 100.00 %

Netline Length  ................ 0 (mm)
Netline Manhattan Length  ...... 0 (mm)
Total Trace Length  ............ 1,656.15 (mm)

Trace Widths Used (mm)  ........ 0.13, 0.13, 0.2, 0.23, 0.25, 0.3, 0.4, 0.5, 0.6, 0.8
Vias  .......................... 401
Via Span  Name                   Quantity
   1-4    0.38VIAh0.2            401

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 165
    Parts Mounted on Top  ...... 140
        SMD  ................... 118
        Through  ............... 5
        Test Points  ........... 17
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 25
        SMD  ................... 0
        Through  ............... 0
        Test Points  ........... 25
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 424
    Holes per Board Area  ...... 0.11 Holes/Sq. (mm)
Mounting Holes  ................ 13
