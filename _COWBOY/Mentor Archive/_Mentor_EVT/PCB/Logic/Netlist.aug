;; V4.1.0
%net
%Prior=1

%page=Schematic1
\$1N521\    \CONN1\-\5\ \FB1\-\2\ 
\$1N602\    \U1\-\18\ \Y1\-\3\ 
\$1N604\    \U1\-\19\ \Y1\-\1\ 
\$1N627\    \FL1\-\4\ \U1\-\12\ 
\$1N652\    \C13\-\1\ \C61\-\2\ \FL1\-\2\ \TP94\-\1\ 
\$1N770\    \C16\-\2\ \U2\-\2\ 
\$1N802\    \C18\-\1\ \D1\-\2\ \U2\-\5\ \U2\-\11\ 
\$1N1097\    \R69\-\1\ \U6\-\2\ 
\$1N1109\    \R6\-\1\ \U3\-\A2\ 
\$1N1111\    \R8\-\1\ \U3\-\C1\ 
\$1N1113\    \R7\-\1\ \U3\-\D1\ 
\$1N1115\    \R5\-\1\ \U3\-\B2\ 
\$1N1117\    \R9\-\1\ \U3\-\B1\ 
\$1N1119\    \R10\-\1\ \U3\-\C2\ 
\$1N2921\    \R45\-\2\ \R50\-\1\ \U2\-\15\ 
\$1N2929\    \R46\-\2\ \R62\-\2\ \U2\-\14\ 
\$1N2937\    \R47\-\2\ \R49\-\1\ \U2\-\7\ 
\$1N2945\    \R48\-\2\ \R61\-\2\ \U2\-\8\ 
\$1N3597\    \C27\-\1\ \C31\-\2\ \L7\-\1\ \R3\-\1\ 
\$1N3619\    \C30\-\1\ \D18\-\1\ \R3\-\2\ 
\$1N3946\    \FB4\-\1\ \U2\-\12\ 
\$1N3954\    \FB3\-\1\ \U2\-\10\ 
\$1N4767\    \CONN1\-\1\ \C62\-\2\ \FB2\-\1\ 
\$1N4934\    \C10\-\1\ \U1\-\38\ 
\$1N5095\    \R53\-\2\ \SW2\-\1\ \SW2\-\2\ 
\$1N5103\    \R52\-\2\ \SW1\-\1\ \SW1\-\2\ 
\$1N5151\    \R5\-\2\ \U1\-\31\ 
\$1N5160\    \R6\-\2\ \U1\-\22\ 
\$1N5169\    \R7\-\2\ \U1\-\26\ 
\$1N5178\    \R8\-\2\ \U1\-\25\ 
\$1N5235\    \D2\-\1\ \D10\-\2\ \R11\-\2\ 
\$1N5258\    \C15\-\2\ \D2\-\4\ \U9\-\3\ 
\$1N5317\    \R67\-\1\ \U1\-\23\ 
\$1N5539\    \D16\-\1\ \R16\-\2\ \U10\-\6\ 
\$1N5552\    \R18\-\2\ \U4\-\3\ \U6\-\3\ \U7\-\3\ 
\$1N5563\    \C28\-\1\ \R16\-\1\ \R17\-\2\ \U7\-\1\ 
\$1N5591\    \R71\-\1\ \U4\-\2\ 
\$1N5742\    \R64\-\1\ \U1\-\32\ 
\$1N6334\    \C91\-\1\ \Q4\-\2\ \R91\-\1\ \R92\-\1\ \R98\-\1\ \TP6\-\1\ 
\$1N6353\    \Q1\-\3\ \R79\-\2\ 
\$1N6979\    \D2\-\3\ \R13\-\2\ 
\$1N6981\    \D2\-\2\ \R12\-\2\ 
\$1N7223\    \C12\-\2\ \U1\-\1\ 
\$1N7230\    \C30\-\2\ \U1\-\67\ 
\$1N7233\    \C27\-\2\ \U1\-\68\ 
\$1N7237\    \ANT1\-\1\ \C61\-\1\ \L14\-\1\ 
\$1N7366\    \C16\-\1\ \U2\-\4\ 
\$1N7794\    \R70\-\2\ \U6\-\1\ 
\$1N7865\    \C24\-\1\ \R19\-\2\ \U1\-\13\ 
\$1N7893\    \C23\-\2\ \R48\-\1\ 
\$1N7895\    \C20\-\2\ \R47\-\1\ 
\$1N7897\    \C21\-\2\ \R46\-\1\ 
\$1N7899\    \C22\-\2\ \R45\-\1\ 
\$1N8171\    \Q6\-\1\ \R83\-\1\ 
\$1N8187\    \C63\-\1\ \C64\-\1\ \D17\-\2\ \FB5\-\1\ \Q6\-\3\ 
\$1N8208\    \FB5\-\2\ \U10\-\1\ 
\$1N8265\    \D18\-\2\ \U1\-\2\ 
\$1N8619\    \C92\-\1\ \Q2\-\1\ \R94\-\1\ \R95\-\1\ \TP7\-\1\ 
\$1N8705\    \Q4\-\1\ \R96\-\2\ \R97\-\1\ 
\$1N8720\    \Q3\-\3\ \R94\-\2\ 
\$1N8775\    \Q2\-\3\ \R98\-\2\ 
\$1N8782\    \Q2\-\2\ \R99\-\1\ 
\$1N8792\    \Q1\-\1\ \Q4\-\3\ 
\AGND\    \C12\-\1\ \C31\-\1\ \C50\-\2\ \C60\-\2\ \C63\-\2\ \Q5\-\1\ 
*  \Q6\-\2\ \R2\-\1\ \R49\-\2\ \R50\-\2\ \R54\-\1\ \R55\-\1\ \TP9\-\1\ 
*  \TP67\-\1\ \TP69\-\1\ \TP75\-\1\ \TP76\-\1\ \TP77\-\1\ \TP78\-\1\ 
*  \U2\-\6\ \U10\-\2\ 
\AGND_DGND\    \Q5\-\2\ \R66\-\1\ \R70\-\1\ 
\AMP_SHDN#\    \R64\-\2\ \R65\-\1\ \R71\-\2\ \TP81\-\1\ \U2\-\16\ 
\BATT_TEMP\    \C25\-\1\ \R1\-\2\ \R14\-\1\ \TP3\-\1\ \U1\-\20\ 
\BLUE\    \D8\-\2\ \R13\-\1\ \R15\-\1\ \U1\-\66\ 
\CSR_RST#\    \Q3\-\1\ \R69\-\2\ \R79\-\1\ \R82\-\2\ \R93\-\2\ \TP60\-\1\ 
*  \TP90\-\1\ \U1\-\35\ 
\GND\    \ANT1\-\2\ \CONN1\-\6\ \CONN1\-\7\ \CONN1\-\8\ \CONN1\-\9\ 
*  \CONN1\-\10\ \CONN1\-\11\ \C1\-\2\ \C2\-\2\ \C3\-\2\ \C4\-\2\ \C5\-\2\ 
*  \C6\-\2\ \C7\-\2\ \C8\-\2\ \C9\-\2\ \C10\-\2\ \C11\-\2\ \C13\-\2\ 
*  \C15\-\1\ \C17\-\2\ \C18\-\2\ \C19\-\2\ \C24\-\2\ \C25\-\2\ \C26\-\2\ 
*  \C28\-\2\ \C29\-\2\ \C32\-\2\ \C33\-\2\ \C46\-\2\ \C62\-\1\ \C65\-\1\ 
*  \C91\-\2\ \C92\-\2\ \D1\-\1\ \D7\-\1\ \D8\-\1\ \D9\-\1\ \D10\-\1\ 
*  \D11\-\1\ \D12\-\1\ \D13\-\1\ \D14\-\2\ \D15\-\2\ \D16\-\2\ \D19\-\1\ 
*  \D20\-\1\ \FB1\-\1\ \FL1\-\1\ \FL1\-\3\ \L14\-\2\ \Q1\-\2\ \Q5\-\3\ 
*  \R2\-\2\ \R14\-\2\ \R15\-\2\ \R65\-\2\ \R80\-\2\ \R81\-\2\ \R92\-\2\ 
*  \R95\-\2\ \R97\-\2\ \R99\-\2\ \SW1\-\5\ \SW2\-\5\ \SW3\-\5\ \TP1\-\1\ 
*  \TP5\-\1\ \TP62\-\1\ \TP73\-\1\ \TP95\-\1\ \U1\-\46\ \U1\-\51\ 
*  \U1\-\69\ \U2\-\3\ \U3\-\D2\ \U7\-\2\ \Y1\-\2\ \Y1\-\4\ 
\GRN\    \D9\-\2\ \R12\-\1\ \U1\-\36\ 
\LX_1V8\    \L1\-\2\ \U1\-\47\ 
\LX_1V35\    \L2\-\2\ \U1\-\50\ 
\MIC+\    \C64\-\2\ \D7\-\2\ \D17\-\1\ \L7\-\2\ \TP32\-\1\ \TP34\-\1\ 
\OUTL_AMP\    \C50\-\1\ \D11\-\2\ \FB4\-\2\ \R55\-\2\ \R62\-\1\ \TP10\-\1\ 
*  \TP28\-\1\ \TP72\-\1\ \U10\-\5\ 
\OUTR_AMP\    \C60\-\1\ \D12\-\2\ \FB3\-\2\ \R54\-\2\ \R61\-\1\ \TP11\-\1\ 
*  \TP27\-\1\ \TP71\-\1\ \U10\-\3\ 
\RED\    \R11\-\1\ \U1\-\37\ 
\SPI_CLK\    \TP56\-\1\ \TP86\-\1\ \U1\-\34\ 
\SPI_CS\    \TP57\-\1\ \TP87\-\1\ \U1\-\24\ 
\SPI_MISO\    \TP58\-\1\ \TP88\-\1\ \U1\-\28\ 
\SPI_MOSI\    \TP59\-\1\ \TP89\-\1\ \U1\-\30\ 
\SPI_PCM\    \R63\-\1\ \TP61\-\1\ \TP91\-\1\ \U1\-\29\ 
\SPKR_LN\    \C21\-\1\ \U1\-\9\ 
\SPKR_LP\    \C22\-\1\ \U1\-\10\ 
\SPKR_RN\    \C23\-\1\ \U1\-\6\ 
\SPKR_RP\    \C20\-\1\ \U1\-\7\ 
\USB_N\    \CONN1\-\2\ \D20\-\2\ \U1\-\55\ 
\USB_P\    \CONN1\-\3\ \D19\-\2\ \U1\-\56\ 
\VBAT\    \C2\-\1\ \C17\-\1\ \C29\-\1\ \C46\-\1\ \R17\-\1\ \R18\-\1\ 
*  \R66\-\2\ \SW3\-\1\ \SW3\-\2\ \TP2\-\1\ \TP55\-\1\ \TP63\-\1\ \U1\-\44\ 
*  \U1\-\45\ \U1\-\48\ \U2\-\1\ \U2\-\9\ \U2\-\13\ \U9\-\1\ 
\VBUS\    \C1\-\1\ \C65\-\2\ \FB2\-\2\ \TP4\-\1\ \TP15\-\1\ \U1\-\42\ 
*  \U9\-\2\ 
\VOL+\    \D15\-\1\ \R81\-\1\ \SW1\-\3\ \SW1\-\4\ \TP83\-\1\ \U1\-\57\ 
\VOL-\    \D14\-\1\ \R80\-\1\ \SW2\-\3\ \SW2\-\4\ \TP84\-\1\ \U1\-\60\ 
\VREG\    \D13\-\2\ \R83\-\2\ \R91\-\2\ \SW3\-\3\ \SW3\-\4\ \TP85\-\1\ 
*  \U1\-\40\ 
\WIRED_AUDIO\    \R67\-\2\ \R68\-\1\ \TP82\-\1\ \TP92\-\1\ \U4\-\1\ 
\1V8\    \C4\-\1\ \C5\-\1\ \C11\-\1\ \C26\-\1\ \L1\-\1\ \Q3\-\2\ \R9\-\2\ 
*  \R10\-\2\ \R52\-\1\ \R53\-\1\ \R63\-\2\ \R68\-\2\ \R82\-\1\ \R93\-\1\ 
*  \R96\-\1\ \TP13\-\1\ \TP65\-\1\ \U1\-\8\ \U1\-\15\ \U1\-\16\ \U1\-\33\ 
*  \U1\-\53\ \U1\-\63\ \U3\-\A1\ 
\1V35\    \C6\-\1\ \C7\-\1\ \C8\-\1\ \C9\-\1\ \C19\-\1\ \C33\-\1\ \L2\-\1\ 
*  \R1\-\1\ \R19\-\1\ \TP12\-\1\ \TP93\-\1\ \U1\-\3\ \U1\-\11\ \U1\-\14\ 
*  \U1\-\17\ \U1\-\39\ \U1\-\52\ 
\3V3_USB\    \C3\-\1\ \C32\-\1\ \TP14\-\1\ \U1\-\41\ \U1\-\49\ \U1\-\54\ 

%Part
\BAT54C\    \U4\ \U6\ \U9\ 
\bead_0402\    \FB1\ \FB2\ \FB3\ \FB4\ \FB5\ 
\BT_invF_small\    \ANT1\ 
\cap_0402\    \C1\ \C2\ \C3\ \C4\ \C5\ \C6\ \C7\ \C8\ \C9\ \C10\ \C11\ 
*  \C12\ \C13\ \C15\ \C16\ \C17\ \C18\ \C19\ \C24\ \C25\ \C26\ \C27\ \C28\ 
*  \C30\ \C31\ \C32\ \C33\ \C46\ \C50\ \C60\ \C61\ \C62\ \C63\ \C64\ \C65\ 
*  \C92\ 
\cap_0603\    \C20\ \C21\ \C22\ \C23\ \C29\ \C91\ 
\CSR8635\    \U1\ 
\CUS520\    \D1\ \D17\ \D18\ 
\C19HE1WT\    \D2\ 
\DMN1019USN-13\    \Q5\ 
\ESD_BI_0402\    \D7\ \D8\ \D9\ \D10\ \D11\ \D12\ \D13\ \D14\ \D15\ \D16\ 
*  \D19\ \D20\ 
\FDV301N\    \U7\ 
\ind_0402\    \L7\ \L14\ 
\ind_0603\    \L1\ \L2\ 
\LFB21_0805\    \FL1\ 
\res_0201\    \R19\ 
\res_0402\    \R1\ \R2\ \R3\ \R5\ \R6\ \R7\ \R8\ \R9\ \R10\ \R11\ \R12\ 
*  \R13\ \R14\ \R15\ \R16\ \R17\ \R18\ \R45\ \R46\ \R47\ \R48\ \R49\ \R50\ 
*  \R52\ \R53\ \R54\ \R55\ \R61\ \R62\ \R63\ \R64\ \R65\ \R66\ \R67\ \R68\ 
*  \R69\ \R70\ \R71\ \R79\ \R80\ \R81\ \R82\ \R83\ \R91\ \R92\ \R93\ \R94\ 
*  \R95\ \R96\ \R97\ \R98\ \R99\ 
\RE1C002UN\    \Q1\ \Q2\ \Q6\ 
\RZM001P02\    \Q3\ \Q4\ 
\SGM4917\    \U2\ 
\TP1\    \TP6\ \TP7\ \TP83\ \TP84\ \TP85\ \TP94\ 
\TP1.5\    \TP1\ \TP4\ \TP5\ \TP10\ \TP11\ \TP12\ \TP13\ \TP14\ \TP15\ 
*  \TP55\ \TP56\ \TP57\ \TP58\ \TP59\ \TP60\ \TP61\ \TP62\ \TP63\ \TP65\ 
*  \TP67\ \TP75\ \TP76\ \TP77\ \TP78\ \TP81\ \TP82\ \TP86\ \TP87\ \TP88\ 
*  \TP89\ \TP90\ \TP91\ \TP92\ \TP93\ \TP95\ 
\TP2\    \TP2\ \TP3\ \TP9\ \TP27\ \TP28\ \TP32\ \TP34\ \TP69\ \TP71\ \TP72\ 
*  \TP73\ 
\TSX-3225\    \Y1\ 
\TS033W1-N35F-03\    \SW1\ \SW2\ \SW3\ 
\uUSB_1050171001\    \CONN1\ 
\W25Q16DW_BY\    \U3\ 
\3_5mm_EJ361D6A_cowboy\    \U10\ 
