.FILETYPE DXF_EXPORT_SETTINGS
.VERSION "VB99.1"
.DATE "09/24/2015 18:03:31"

!  This file contains settings for the most recent DXF export session

.UNITS mm

.SCHEME "TopMount"
 ..SCHEME_LOCATION Local

.FILL_PADS 0

.BOARD_CONTENTS 0

.MIRROR_OUTPUT 0

.ITEM "Assembly Outlines Top"
 ..LAYER "Assembly_Outlines_Top"
 ..EXPORT 1

.ITEM "Assembly Part Numbers Top"
 ..LAYER "Assembly_Part_Numbers_Top"
 ..EXPORT 1

.ITEM "Assembly Ref Des Top"
 ..LAYER "Assembly_Ref_Des_Top"
 ..EXPORT 1

.ITEM "Board Outline"
 ..LAYER "Board_Outline"
 ..EXPORT 1

.ITEM "DXF_Board_outline"
 ..LAYER "DXF_Board_outline"
 ..EXPORT 1

.ITEM "DXF_0"
 ..LAYER "DXF_0"
 ..EXPORT 1

.ITEM "Default User Layer"
 ..LAYER "Default_User_Layer"
 ..EXPORT 1

.ITEM "Mounting Holes Layer 1"
 ..LAYER "Mounting_Holes_Layer_Top"
 ..EXPORT 1

.ITEM "Part Holes Layer 1"
 ..LAYER "Part_Holes_Layer_Top"
 ..EXPORT 1

.ITEM "Part Pads Layer 1"
 ..LAYER "Part_Pads_Layer_Top"
 ..EXPORT 1

.ITEM "Part Pads SMD Top"
 ..LAYER "Part_Pads_SMD_Top"
 ..EXPORT 1

.ITEM "Placement Outlines Top"
 ..LAYER "Placement_Outlines_Top"
 ..EXPORT 1

.ITEM "Silkscreen Outlines Top"
 ..LAYER "Silkscreen_Outlines_Top"
 ..EXPORT 1

.ITEM "Silkscreen Ref Des Top"
 ..LAYER "Silkscreen_Ref_Des_Top"
 ..EXPORT 1

.ITEM "Soldermask Pads Top"
 ..LAYER "Soldermask_Pads_Top"
 ..EXPORT 1

.ITEM "Solderpaste Pads Top"
 ..LAYER "Solderpaste_Pads_Top"
 ..EXPORT 1

