=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_45.txt

Wed Jan 27 14:07:31 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.97 X 61.11 (mm)
Route Border Extents  .......... 55.97 X 61.11 (mm)
Actual Board Area  ............. 1,674.55 (mm)
Actual Route Area  ............. 1,674.55 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,349.1 Sq. (mm)  1,234.32 Sq. (mm) 36.86 %

Pins  .......................... 515
Pins per Route Area  ........... 0.31 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 1, 1.5
    Layer 2 is a signal layer
        Trace Widths  .......... 0.13, 0.2
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.4, 0.5, 0.7, 1.4

Nets  .......................... 126
Connections  ................... 429
Open Connections  .............. 101
Differential Pairs  ............ 0
Percent Routed  ................ 76.46 %

Netline Length  ................ 487.53 (mm)
Netline Manhattan Length  ...... 582.73 (mm)
Total Trace Length  ............ 1,928.86 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7, 1, 1.4, 1.5
Vias  .......................... 230
Via Span  Name                   Quantity
   1-4    026VIA                 5
          0.3Via0.15h            7
          0.3VIAh0.15            26
          0.38VIAh0.2            128
          0.38VIAh0.25           64

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 197
    Parts Mounted on Top  ...... 130
        SMD  ................... 108
        Through  ............... 1
        Test Points  ........... 21
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 67
        SMD  ................... 53
        Through  ............... 1
        Test Points  ........... 13
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 253
    Holes per Board Area  ...... 0.15 Holes/Sq. (mm)
Mounting Holes  ................ 14
