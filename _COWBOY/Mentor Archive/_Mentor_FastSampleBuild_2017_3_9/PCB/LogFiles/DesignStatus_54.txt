=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:        C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\

Design Status Report: C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\DesignStatus_54.txt

Sun Feb 07 01:59:15 2016

=======================================================================
DESIGN STATUS
=======================================================================
Board Size Extents  ............ 55.97 X 60.75 (mm)
Route Border Extents  .......... 55.97 X 60.75 (mm)
Actual Board Area  ............. 1,684.72 (mm)
Actual Route Area  ............. 1,684.72 (mm)

Placement Areas: Name            Available         Required          Required/Available
                 Entire Board    3,369.44 Sq. (mm) 1,167.35 Sq. (mm) 34.65 %

Pins  .......................... 526
Pins per Route Area  ........... 0.31 Pins/Sq. (mm)

Layers  ........................ 4
    Layer 1 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 1, 1.5
    Layer 2 is a signal layer
        Trace Widths  .......... 0.13, 0.2
    Layer 3 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3
    Layer 4 is a signal layer
        Trace Widths  .......... 0.1, 0.13, 0.13, 0.2, 0.3, 0.4, 0.5, 0.7, 1.4

Nets  .......................... 127
Connections  ................... 434
Open Connections  .............. 135
Differential Pairs  ............ 0
Percent Routed  ................ 68.89 %

Netline Length  ................ 447.84 (mm)
Netline Manhattan Length  ...... 533.63 (mm)
Total Trace Length  ............ 2,311.31 (mm)

Trace Widths Used (mm)  ........ 0.1, 0.13, 0.13, 0.14, 0.16, 0.2, 0.23, 0.25, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7, 1, 1.4, 1.5
Vias  .......................... 236
Via Span  Name                   Quantity
   1-4    026VIA                 5
          0.3VIAh0.15            24
          0.3Via0.15h            8
          0.38VIAh0.25           56
          0.38VIAh0.2            143

Teardrops....................... 0
    Pad Teardrops............... 0
    Trace Teardrops............. 0
    Custom Teardrops............ 0
Tracedrops...................... 0

Virtual Pins.................... 0
Guide Pins ..................... 0

Parts Placed  .................. 202
    Parts Mounted on Top  ...... 105
        SMD  ................... 83
        Through  ............... 1
        Test Points  ........... 21
        Mechanical  ............ 0
    Parts Mounted on Bottom  ... 97
        SMD  ................... 82
        Through  ............... 0
        Test Points  ........... 15
        Mechanical  ............ 0
    Embedded Components ........ 0
        Capacitors ............. 0
        Resistors .............. 0
    Edge Connector Parts  ...... 0

Parts not Placed  .............. 0

Nested Cells  .................. 0

Jumpers  ....................... 0

Through Holes  ................. 251
    Holes per Board Area  ...... 0.15 Holes/Sq. (mm)
Mounting Holes  ................ 11
