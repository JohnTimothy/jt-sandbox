=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:    C:\Users\branden.sheffield\Documents\HeshBT\Mentor\LHBT120Main\PCB\


Design Units:(mm)

Test Point Log:   C:\Users\branden.sheffield\Documents\HeshBT\Mentor\LHBT120Main\PCB\LogFiles\testptlog_03.txt

Fri Apr 29 09:59:55 2016

Net Name        Ref                  X          Y     Side    Location        Operation Result
==============================================================================================
AGND            TP75                 -4.95     -26.43 Bottom  Trace           Delete              
AGND            TP75                 -4.95     -26.43 Bottom  Trace           Delete              
AGND            TP9                 -19.72     -22.69 Top     Via             Delete              
AGND            TP9                 -19.72     -22.69 Top     Via             Delete              
