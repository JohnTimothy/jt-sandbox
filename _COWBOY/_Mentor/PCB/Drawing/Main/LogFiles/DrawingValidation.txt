=======================================================================
PADS Drawing Editor - Version 116.0.506.1217
=======================================================================

Job Directory:        C:\pd_repo\pd\_COWBOY\_Mentor\PCB\Drawing\Main\

 Drawing Validation Log: C:\pd_repo\pd\_COWBOY\_Mentor\PCB\Drawing\Main\LogFiles\DrawingValidation.txt

Fri Jan 20 18:00:15 2017

==============================================================================================================================================

Validating design: ..\..\Main.pcb

The design has some elements outside the Manufacturing Outline.These elements would be ignored in the Drawing 
Please extend the Manufacturing Outline in the Layout Editing tool to include these hanging objects in the Drawing
NOTE: This check is being done for all the objects except that of the User Draft Layers and Drill Drawing-Thru
