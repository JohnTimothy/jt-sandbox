=======================================================================
PADS Professional Layout - Version 2015.0.678.963
=======================================================================

Job Directory:    C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\


Design Units:(mm)

Test Point Log:   C:\Users\branden.sheffield\Documents\Crusher3.0\Mentor\CrusherMain\PCB\LogFiles\testptlog_01.txt

Thu Sep 24 15:33:53 2015

Net Name        Ref                  X          Y     Side    Location        Operation Result
==============================================================================================
1V8             TP19                  12.1       4.93 Bottom  Hanger          Delete              
1V8             TP19                  12.1       4.93 Bottom  Hanger          Delete              
1V35            TP20                 15.02       5.45 Bottom  Hanger          Delete              
1V35            TP20                 15.02       5.45 Bottom  Hanger          Delete              
VBUS            TP18                  9.43       5.61 Bottom  Hanger          Delete              
VBUS            TP18                  9.43       5.61 Bottom  Hanger          Delete              
